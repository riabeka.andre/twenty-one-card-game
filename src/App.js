import React from 'react';

import './App.css';
import {connect} from "react-redux";
import carts from "./components/carts/Carts";
import Deck from "./components/deck/Deck";
import PlayerCarts from "./components/player/PlayerCarts";
import ComputerCarts from "./components/computer/ComputerCarts";

class App extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            sum: 0,
            sobi: false,
            compSum: 0,
            battleIsFinished: false
        }
    }
    componentDidMount() {


    }
    componentDidUpdate(prevProps, prevState, snapshot) {

    }

toGiveCarts = () =>{
    this.props.onLoadPlayerCarts(this.props.deck[0]);
    this.props.onRemoveFromDeck(this.props.deck[0]);
    this.props.onLoadComputerCarts(this.props.deck[1]);
    this.props.onRemoveFromDeck(this.props.deck[1]);
    this.setState({
        sum: this.props.player[0].rang
    });
    this.setState({
        compSum: this.props.computer[0].rang
    });
    this.props.onSetPlayerScore(this.props.deck[0].rang);
    this.props.onSetCompScore(this.props.deck[1].rang);
    }
toFormDeck =()=>{
    let currentCarts = carts;
    for(let i = 0; i < 36; i++){
        let rand =  Math.floor(Math.random() * (carts.length));
        this.props.onLoadDeck(carts[rand]);
        currentCarts.splice(rand, 1)
    }
}
    onLetsPlayNewGame = () => {
if(this.props.deck.length === 0){

    setTimeout(this.toFormDeck, 5);
    setTimeout(this.toGiveCarts, 10);

}


    };
    onOneMoreCart = () =>{
        if(this.props.playerScore.length !== 0){
            this.props.onLoadPlayerCarts(this.props.deck[0]);
            this.props.onRemoveFromDeck(this.props.deck[0]);
            let {sum} = this.state;
            sum = sum + this.props.deck[0].rang;
            this.setState({
                sum: sum
            });
            this.props.onSetPlayerScore(sum);
        }
        else return false;

    };
    toSobi = () => {
        this.setState({
            sobi: true
        });
    };
    compTakeCart = () => {
        this.props.onLoadComputerCarts(this.props.deck[0]);
        this.props.onRemoveFromDeck(this.props.deck[0]);
        let {compSum} = this.state;
        console.log(this.props.computer)
        compSum = compSum + this.props.computer[this.props.computer.length - 1].rang
        this.setState({
            compSum: compSum
        });
        this.props.onSetCompScore(compSum);
        console.log(this.state.compSum)
        return this.state.compSum;
    };
    func1 = () =>{
        if(this.state.compSum < 17 ){
            this.props.onLoadComputerCarts(this.props.deck[0]);
            this.props.onRemoveFromDeck(this.props.deck[0]);
            let {compSum} = this.state;
            compSum = compSum + this.props.computer[this.props.computer.length - 1].rang
            this.setState({
                compSum: compSum
            });
            this.props.onSetCompScore(compSum);
            return this.state.compSum;
        }
        else return false
    }
    func2 = () =>{
        if(this.state.compSum < 17 ){
            setTimeout(this.func1, 2004);
        }
        else return false
    }
    func3 = () =>{
        if(this.state.compSum < 17 ){
            setTimeout(this.func1, 2004);
        }
        else return false
    }
    self = () => {
        if(this.props.computer.length !== 0){
            try{
                setTimeout(this.toSobi, 1000);
                setTimeout(this.compTakeCart, 3001);
                setTimeout(this.func1, 5002);
                setTimeout(this.func2, 6003);
                setTimeout(this.func3, 6003);
            }finally {
                this.setState({
                    battleIsFinished: true
                })
            }
        }
        return false
    };
    setFinishedIsFalse = () => {
        this.setState({
            battleIsFinished: false,
            sum: 0,
            compSum: 0
        })
    }

    render() {
    const won = ((this.state.battleIsFinished && (((this.props.player[1] !== undefined && (this.props.player[0].rang && this.props.player[1].rang === 11)))
        || (this.props.compScore > 21) )) || (this.state.sum === 21))  &&
        <div className='banner'> YOU WON WITH THE SCORE {this.state.sum}<br/> COMPUTER SCORE IS SCORE {this.state.compSum} <div className='close' onClick={this.setFinishedIsFalse}>X</div></div>
    const lost = (this.state.battleIsFinished && (((this.state.sum < 21 && this.state.compSum < 21) && (this.state.compSum > this.state.sum && this.state.sobi !== false)
        && this.state.compSum > 16) || this.state.compSum === 21 || (this.state.compSum === this.state.sum)) ||  (this.state.sum > 21)) &&
        <div className='banner'> YOU LOST WIT THE SCORE {this.state.sum}<div className='close' onClick={this.setFinishedIsFalse}>X</div></div>;

    return (
        <div className="App">
            <div>{won}{lost}</div>
            <button onClick={this.onLetsPlayNewGame}>Давай зіграємо нову гру</button>
            <div className='buttons-ok'>
                <button className='button-ok' onClick={this.onOneMoreCart}>Ще карту</button>
                <button className='button-ok' onClick={this.self}>Собі</button>
            </div>

            <div className="player"> <PlayerCarts/></div>
            <div className='words'>
                <div>кількість ваших очків - {this.state.sum}</div>
                <div>кількість очків комп'ютера - {this.state.sobi === true ? this.state.compSum : ":)"}</div>
            </div>

            <div className="deck"> <Deck/></div>
            <div className="comp-carts"> <ComputerCarts sobi={this.state.sobi}/></div>
        </div>
    );
  }


}

export default connect(
    state => ({
        player: state.player,
        computer: state.computer,
        deck: state.deck,
        discard: state.discard,
        battleIsFinished: state.battleIsFinished,
        playerAct: state.playerAct,
        playerScore: state.playerScore,
        compScore: state.compScore
    }),
    dispatch => ({
      onLoadPlayerCarts: (item) => {
        dispatch({type: 'LOAD_PLAYER_CARTS', payload: item})
      },
      onLoadComputerCarts: (item) => {
        dispatch({type: 'LOAD_COMPUTER_CARTS', payload: item})
      },
      onLoadDeck: (item) => {
        dispatch({type: 'LOAD_DECK', payload: item})
      },
      onRemoveFromCompCarts: (item) => {
        dispatch({type: 'REMOVE_COMPUTER_CARTS', payload: item})
      },
      onDiscard: (item) => {
        dispatch({type: 'MOVE_TO_DISCARD', payload: item})
      },
      onRemoveFromDeck: (item) => {
        dispatch({type: 'REMOVE_FROM_DECK', payload: item})
      },
        onCleanDeck: () => {
            dispatch({type: 'CLEAN_DECK'})
        },
      onBattleIsFinished: (item) => {
        dispatch({type: 'BATTLE_IS_FINISHED', payload: item})
      },
      onPlayerAct: (item) => {
        dispatch({type: 'FIRST_PLAYER_ACT', payload: item})
      },
      onNewGame: () => {
        dispatch({type: 'NEW_GAME'})
      },
      onSetPlayerScore: (item) => {
        dispatch({type: 'SET_PLAYER_SCORE', payload: item})
      },
      onSetCompScore: (item) => {
        dispatch({type: 'SET_COMP_SCORE', payload: item})
      },
    })
)(App);
