
let carts = [

    {
        id: 28,
        suit: 'heart',
        rang: 6,
        src: 'carts/heart/sixheart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 29,
        suit: 'heart',
        rang: 7,
        src: 'carts/heart/sevenHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 30,
        suit: 'heart',
        rang: 8,
        src: 'carts/heart/eightHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 31,
        suit: 'heart',
        rang: 9,
        src: 'carts/heart/nineHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 32,
        suit: 'heart',
        rang: 10,
        src: 'carts/heart/tenHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 33,
        suit: 'heart',
        rang: 2,
        src: 'carts/heart/jackHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 34,
        suit: 'heart',
        rang: 3,
        src: 'carts/heart/queenHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 35,
        suit: 'heart',
        rang: 4,
        src: 'carts/heart/kingHeart.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 36,
        suit: 'heart',
        rang: 11,
        src: 'carts/heart/tuse.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 1,
        suit: 'spade',
        rang: 6,
        src: 'carts/spade/six-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 2,
        suit:'spade',
        rang: 7,
        src: 'carts/spade/seven-apde.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 3,
        suit: 'spade',
        rang: 8,
        src: 'carts/spade/eight-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 4,
        suit: 'spade',
        rang: 9,
        src: 'carts/spade/nine-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 5,
        suit: 'spade',
        rang: 10,
        src: 'carts/spade/ten-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 6,
        suit: 'spade',
        rang: 2,
        src: 'carts/spade/jack-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 7,
        suit: 'spade',
        rang: 3,
        src: 'carts/spade/qeen-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 8,
        suit: 'spade',
        rang: 4,
        src: 'carts/spade/king-spade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 9,
        suit: 'spade',
        rang: 11,
        src: 'carts/spade/holeinoneSpade.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 10,
        suit: 'diamonds',
        rang: 6,
        src: 'carts/diamonds/six-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 11,
        suit: 'diamonds',
        rang: 7,
        src: 'carts/diamonds/seven-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 12,
        suit: 'diamonds',
        rang: 8,
        src: 'carts/diamonds/eight-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 13,
        suit: 'diamonds',
        rang: 9,
        src: 'carts/diamonds/nine-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 14,
        suit: 'diamonds',
        rang: 10,
        src: 'carts/diamonds/ten-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 15,
        suit: 'diamonds',
        rang: 2,
        src: 'carts/diamonds/jack-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 16,
        suit: 'diamonds',
        rang: 3,
        src: 'carts/diamonds/queen-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 17,
        suit: 'diamonds',
        rang: 4,
        src: 'carts/diamonds/king-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 18,
        suit: 'diamonds',
        rang: 11,
        src: 'carts/diamonds/ace-diamonds.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 19,
        suit: 'clubs',
        rang: 6,
        src: 'carts/clubs/six-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 20,
        suit: 'clubs',
        rang: 7,
        src: 'carts/clubs/seven-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 21,
        suit: 'clubs',
        rang: 8,
        src: 'carts/clubs/eight-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 22,
        suit: 'clubs',
        rang: 9,
        src: 'carts/clubs/nine-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 23,
        suit: 'clubs',
        rang: 10,
        src: 'carts/clubs/ten-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 24,
        suit: 'clubs',
        rang: 2,
        src: 'carts/clubs/jack-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 25,
        suit: 'clubs',
        rang: 3,
        src: 'carts/clubs/queen-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 26,
        suit: 'clubs',
        rang: 4,
        src: 'carts/clubs/king-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
    {
        id: 27,
        suit: 'clubs',
        rang: 11,
        src: 'carts/clubs/ace-clubs.png',
        back: 'carts/back.png',
        battle: false
    },
];
export default carts