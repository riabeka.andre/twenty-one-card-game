import React from "react";
import {connect} from "react-redux";
class Cart extends React.Component{


    render() {
        return(
            <div><img className='cart' src={this.props.src} onClick={this.toAttack} alt='playerCart'/></div>
        )
    }
}
export default connect(
    state => ({
        player: state.player,
        computer: state.computer,
        deck: state.deck,
        playerAct: state.playerAct,
    }),
    dispatch => ({
        onAddToBattleField: (item) => {
            dispatch({type: 'ADD_TO_BATTLEFIELD', payload: item})
        },
        onRemoveFromPlayer: (item) => {
            dispatch({type: 'REMOVE_FROM_PLAYER', payload: item})
        },
        onBattleIsFinished: (item) => {
            dispatch({type: 'BATTLE_IS_FINISHED', payload: item})
        },
    })
)(Cart)