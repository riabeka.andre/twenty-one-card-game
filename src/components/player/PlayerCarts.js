import React from "react";
import Cart from "./Cart";
import {connect} from "react-redux";
class PlayerCarts extends React.Component {

   componentDidMount() {

   }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }



    render() {
        return(
            this.props.player.map(item => {
                return(
                    <Cart src={item.src}
                                     key={item.id}
                                     item={item}/>
                )

            })

        )
    }

}
export default connect(
    state => ({
        player: state.player,
        computer: state.computer,
        deck: state.deck,
        discard: state.discard,
        battleIsFinished: state.battleIsFinished,
        playerAct: state.playerAct,
        playerScore: state.playerScore,
        compScore: state.compScore
    }),
    dispatch => ({
        onLoadPlayerCarts: (item) => {
            dispatch({type: 'LOAD_PLAYER_CARTS', payload: item})
        },
        onLoadComputerCarts: (item) => {
            dispatch({type: 'LOAD_COMPUTER_CARTS', payload: item})
        },
        onLoadDeck: (item) => {
            dispatch({type: 'LOAD_DECK', payload: item})
        },
        onRemoveFromCompCarts: (item) => {
            dispatch({type: 'REMOVE_COMPUTER_CARTS', payload: item})
        },
        onDiscard: (item) => {
            dispatch({type: 'MOVE_TO_DISCARD', payload: item})
        },
        onRemoveFromDeck: (item) => {
            dispatch({type: 'REMOVE_FROM_DECK', payload: item})
        },
        onCleanDeck: () => {
            dispatch({type: 'CLEAN_DECK'})
        },
        onCleanPlayer: () => {
            dispatch({type: 'CLEAN_PLAYER'})
        },
        onCleanComp: () => {
            dispatch({type: 'CLEAN_COMP'})
        },
        onBattleIsFinished: (item) => {
            dispatch({type: 'BATTLE_IS_FINISHED', payload: item})
        },
        onPlayerAct: (item) => {
            dispatch({type: 'FIRST_PLAYER_ACT', payload: item})
        },
        onNewGame: () => {
            dispatch({type: 'NEW_GAME'})
        },
        onSetPlayerScore: (item) => {
            dispatch({type: 'SET_PLAYER_SCORE', payload: item})
        },
        onSetCompScore: (item) => {
            dispatch({type: 'SET_COMP_SCORE', payload: item})
        },
    })
)(PlayerCarts)