import React from "react";
import Cart from "../computer/Cart";
import {connect} from "react-redux";

class ComputerCarts extends React.Component {

componentDidUpdate(prevProps, prevState, snapshot) {




}

    render() {
        return(
            this.props.computer.map(item => {
                return(
                    <Cart src={item.src}
                                 key={item.id}
                    back={item.back}
                    sobi={this.props.sobi}/>
                )
            })

        )
    }
}
export default connect(
    state => ({
        player: state.player,
        computer: state.computer,
        deck: state.deck,
        discard: state.discard,
        battleIsFinished: state.battleIsFinished,
        playerAct: state.playerAct,
        playerScore: state.playerScore,
        compScore: state.compScore
    }),
    dispatch => ({
        onLoadFirstPlayerCarts: (item) => {
            dispatch({type: 'LOAD_FIRST_PLAYER_CARTS', payload: item})
        },
        onLoadComputerCarts: (item) => {
            dispatch({type: 'LOAD_COMPUTER_CARTS', payload: item})
        },
        onLoadDeck: (item) => {
            dispatch({type: 'LOAD_DECK', payload: item})
        },
        onLoadTrump: (item) => {
            dispatch({type: 'LOAD_TRUMP', payload: item})
        },
        onRemoveFromComCarts: (item) => {
            dispatch({type: 'REMOVE_COMPUTER_CARTS', payload: item})
        },
        onAddToBattleField: (item) => {
            dispatch({type: 'ADD_TO_BATTLEFIELD', payload: item})
        },
        onRemoveFromBattleField: (item) => {
            dispatch({type: 'REMOVE_FROM_BATTLEFIELD', payload: item})
        },
        onDiscard: (item) => {
            dispatch({type: 'MOVE_TO_DISCARD', payload: item})
        },
        onRemoveFromDeck: (item) => {
            dispatch({type: 'REMOVE_FROM_DECK', payload: item})
        },
        onBattleIsFinished: (item) => {
            dispatch({type: 'BATTLE_IS_FINISHED'})
        },
        onFirstPlayerAct: (item) => {
            dispatch({type: 'FIRST_PLAYER_ACT'})
        }
    })
)(ComputerCarts);