import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";

const initialState = {
    player: [],
    computer: [],
    deck: [],
    discard: [],
    battleIsFinished: false,
    playerAct: true,
    playerScore: [],
    compScore: [],

};

function reducer (state = initialState, action) {
    if (action.type === 'NEW_GAME') {
        const playerCurrentState = [...state.player];
        playerCurrentState.splice(0,);
        const compCurrentState = [...state.computer];
        compCurrentState.splice(0);
        const deckCurrentState = [...state.deck];
        deckCurrentState.splice(0);
        const discardCurrentState = [...state.discard]
        discardCurrentState.splice(0)
        return {
            ...state,
            player: playerCurrentState,
            computer: compCurrentState,
            deck: deckCurrentState,
            discard: discardCurrentState,
        }
    }else if (action.type === 'LOAD_PLAYER_CARTS') {
        return {
            ...state,
            player: [
                ...state.player,
                action.payload
            ]
        }
    }else if (action.type === 'REMOVE_FROM_PLAYER') {
        const currentState = [...state.player];
        const idx = state.player.findIndex(fav => fav.id === action.payload.id)
        currentState.splice(idx ,1);
        return {
            ...state,
            player:  currentState
        }
    }else if (action.type === 'CLEAN_PLAYER') {
        const currentState = [...state.player];
        currentState.splice(0 );
        return {
            ...state,
            player:  currentState
        }
    }else if (action.type === 'CLEAN_COMP') {
        const currentState = [...state.compter];
        currentState.splice(0);
        return {
            ...state,
            computer: currentState
        }
    }else if (action.type === 'CLEAN_DECK') {
        const currentState = [...state.deck];
        // const idx = state.player.findIndex(fav => fav.id === action.payload.id)
        currentState.splice(0 );
        return {
            ...state,
            deck:  currentState
        }
    }else if (action.type === 'LOAD_COMPUTER_CARTS') {
        return {
            ...state,
            computer: [
                ...state.computer,
                action.payload
            ]
        }
    }else if (action.type === 'REMOVE_COMPUTER_CARTS') {
        const currentState = [...state.computer];
        const idx = state.computer.findIndex(fav => fav.id === action.payload.id)
        currentState.splice(idx ,1);
        return {
            ...state,
            computer:  currentState
        }
    } else if (action.type === 'LOAD_DECK') {
        return {
            ...state,
            deck: [
                ...state.deck,
                action.payload
            ]
        }
    }else if (action.type === 'REMOVE_FROM_DECK') {
        const currentState = [...state.deck];
        const idx = state.deck.findIndex(fav => fav.id === action.payload.id)
        currentState.splice(idx ,1);
        return {
            ...state,
            deck:  currentState
        }
    }else if (action.type === 'MOVE_TO_DISCARD') {
        return {
            ...state,
            discard: [
                ...state.discard,
                action.payload
            ]
        }
    }else if (action.type === 'BATTLE_IS_FINISHED') {
        return {
            ...state,
            battleIsFinished: action.payload

        }
    }else if (action.type === 'PLAYER_ACT') {
        return {
            ...state,
            firstPlayerAct: action.payload

        }
    }else if (action.type === 'SET_PLAYER_SCORE') {
        return {
            ...state,
            playerScore: [
                action.payload
            ]
        }
    }else if (action.type === 'SET_COMP_SCORE') {
        return {
            ...state,
            compScore: [
                action.payload
            ]
        }
    }
    return state

}
const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
